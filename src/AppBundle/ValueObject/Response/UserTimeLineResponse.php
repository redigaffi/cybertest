<?php 
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 26/07/17
 * Time: 18:31
 */

namespace AppBundle\ValueObject\Response;

use AppBundle\Interfaces\Response\Response;

class UserTimeLineResponse implements Response
{
	private $message;

	public function __construct(string $message)
	{
		$this->message = $message;
	}

	public static function fromArray(array $response): UserTimeLineResponse
	{
		return new self($response['text']);
	}
}