<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 26/07/17
 * Time: 18:31
 */

namespace AppBundle\ValueObject\Response;

use AppBundle\Interfaces\Response\Response;
use AppBundle\Interfaces\Response\ResponsCollectionInterface;

class ResponseCollection implements ResponsCollectionInterface
{
	private $collection = [];

	public function add(Response $response)
	{
		$this->collection[] = $response;
	}

	public function getAll(): array
	{
		return $this->collection;
	}

	public function getBy(int $position): Response
	{
		return $this->collecton[$position];
	}

	public function getCount(): int
	{
		return count($this->collection);
	}

}