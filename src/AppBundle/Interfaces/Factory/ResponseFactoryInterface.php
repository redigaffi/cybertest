<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 26/07/17
 * Time: 18:31
 */

namespace AppBundle\Interfaces\Factory;

use AppBundle\Interfaces\Response\Response;
use AppBundle\Interfaces\Request\Request;

interface ResponseFactoryInterface
{
	public function getResponse(Request $request, array $response): Response;
}