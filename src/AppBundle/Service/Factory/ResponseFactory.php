<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 26/07/17
 * Time: 18:31
 */

namespace AppBundle\Service\Factory;

use AppBundle\Interfaces\Request\Request;
use AppBundle\Interfaces\Response\Response;
use AppBundle\ValueObject\Request\UpdateStatus;
use AppBundle\ValueObject\Request\UserTimeLine;
use AppBundle\ValueObject\Response\UpdateStatusResponse;
use AppBundle\ValueObject\Response\UserTimeLineResponse;
use AppBundle\Interfaces\Factory\ResponseFactoryInterface;

class ResponseFactory implements ResponseFactoryInterface
{
	public function getResponse(Request $request, array $response): Response
	{
		$className = get_class($request);
		
		switch ($className) {
			case UpdateStatus::class:
				return UpdateStatusResponse::fromArray($response);

			case UserTimeLine::class:
				return UserTimeLineResponse::fromArray($response);

		}
		
	}
}