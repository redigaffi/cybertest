<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 26/07/17
 * Time: 18:31
 */

namespace AppBundle\Service;

use AppBundle\Interfaces\ProcessApiRequest;
use AppBundle\Interfaces\Request\Request;
use AppBundle\Interfaces\Request\Response;
use AppBundle\Exception\WrongRequestTypeException;
use AppBundle\Service\Factory\ResponseFactory;
use AppBundle\Interfaces\Factory\ResponseFactoryInterface;
use AppBundle\Interfaces\Response\ResponsCollectionInterface;
use AppBundle\ValueObject\Response\ResponseCollection;

class TwitterApiService implements ProcessApiRequest
{
	private $twitterApiExchange;
	private $responseFactory;

	public function __construct($twitterApiExchange, ResponseFactoryInterface $responseFactory)
	{
		$this->twitterApiExchange = $twitterApiExchange;
		$this->responseFactory = $responseFactory;
	}

	public function processOperation(Request $request): ResponseCollection
	{
		$responseArray = json_decode($this->processOperationByRequestType($request), true);
		return $this->parseResponseCollection($request, $responseArray);
	}

	private function parseResponseCollection(Request $request, array $response): ResponseCollection
	{
		$responseCollection = new ResponseCollection();
	
		if (!array_key_exists(0, $response)) {
			$responseParsed = $this->responseFactory->getResponse($request, $response);
			$responseCollection->add($responseParsed);
			return $responseCollection;
		}
		
		foreach ($response as $rs) {
			$responseParsed = $this->responseFactory->getResponse($request, $rs);
			$responseCollection->add($responseParsed);
		} 
		
		return $responseCollection;

	}

	private function processOperationByRequestType(Request $request): string
	{
		
		switch ($request->getRequestType()) {
				case Request::TYPE_GET:
					return $this->processGetRequest($request);

				case Request::TYPE_POST:
					return $this->processPostRequest($request);

		}

		throw new WrongRequestTypeException();
	}

	private function processGetRequest(Request $request): string
	{
		$urlData = http_build_query($request->getRawParams());
		$getParamsAsString = '?' . $urlData;
		
		return $this->twitterApiExchange
							->setGetfield($getParamsAsString)
                            ->buildOauth($request->getEndPoint(), Request::TYPE_GET)
                            ->performRequest();

	}

	private function processPostRequest(Request $request): string
	{
		return $this->twitterApiExchange
							->buildOauth($request->getEndPoint(), Request::TYPE_POST)
             				->setPostfields($request->getRawParams())
             				->performRequest();
	}

}