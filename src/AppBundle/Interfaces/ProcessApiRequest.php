<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 26/07/17
 * Time: 18:31
 */

namespace AppBundle\Interfaces;

use AppBundle\Interfaces\Request\Request;
use AppBundle\ValueObject\Response\ResponseCollection;

interface ProcessApiRequest
{
	public function processOperation(Request $request): ResponseCollection;
}