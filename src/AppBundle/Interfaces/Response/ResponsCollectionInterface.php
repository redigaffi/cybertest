<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 26/07/17
 * Time: 18:31
 */

namespace AppBundle\Interfaces\Response;

interface ResponsCollectionInterface
{
	public function add(Response $response);
	public function getAll(): array;
	public function getBy(int $position): Response;
	public function getCount(): int;
}