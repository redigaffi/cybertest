<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 26/07/17
 * Time: 18:31
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\ValueObject\Request\UserTimeLine;
use AppBundle\ValueObject\Request\UpdateStatus;

class ApiController extends Controller
{
   
    public function indexAction(Request $request)
    {
        $twitterApiService = $this->get('app.twitter_api_service');
        $ut = new UserTimeLine("CyberTest__", 5);
        $result = $twitterApiService->processOperation($ut);

        //$us = new UpdateStatus("Otra prueba 28");
        //$result = $twitterApiService->processOperation($us);
        dump($result);
               
        return new Response();
    }
}
