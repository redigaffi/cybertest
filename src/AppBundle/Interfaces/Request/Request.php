<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 26/07/17
 * Time: 18:31
 */

namespace AppBundle\Interfaces\Request;

interface Request 
{
	const TYPE_GET 	= 'GET';
	const TYPE_POST 	= 'POST';

	public function getEndPoint(): string;
	public function getRequestType(): string;
	public function getRawParams(): array;
}