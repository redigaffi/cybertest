<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 27/07/17
 * Time: 16:26
 */

namespace AppBundle\Exception;

class ErrorParsingResponseException extends \Exception
{
    protected $code = -2;

    public function __construct(String $message = "")
    {
        parent::__construct($message, $this->code, null);
    }
}