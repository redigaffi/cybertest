<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 26/07/17
 * Time: 18:31
 */

namespace AppBundle\ValueObject\Request;

use AppBundle\Interfaces\Request\Request;

class UpdateStatus implements Request
{
	private $status;

	public function __construct(string $status)
	{
		$this->status = $status;
	}

	public function getEndPoint(): string
	{
		return 'https://api.twitter.com/1.1/statuses/update.json';
	}

	public function getRequestType(): string
	{
		return Request::TYPE_POST;
	}

	public function getRawParams(): array
	{
		return [
			'status' => $this->status
		];
	}
}