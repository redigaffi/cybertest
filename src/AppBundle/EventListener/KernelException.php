<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 26/07/17
 * Time: 18:31
 */

namespace AppBundle\EventListener;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;

class KernelException
{
	private $logger;
	private $environment;

	public function __construct(LoggerInterface $logger, string $environment)
	{
		$this->logger = $logger;
		$this->environment = $environment;
	}
	
	public function onKernelException(GetResponseForExceptionEvent $event)
	{
        /**
         * Set new Response only in PROD environment, in dev environment we want to use Symfony  debugger
         */
        if ('prod' === $this->environment) {
            $exception = $event->getException();
            $responseContent = '<h1>An error happened: '.$exception->getMessage().'</h1>';
            $newResponse = new Response($responseContent);
            $event->setResponse($newResponse);
        }

	}
}