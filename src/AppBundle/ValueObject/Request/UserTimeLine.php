<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 26/07/17
 * Time: 18:31
 */

namespace AppBundle\ValueObject\Request;

use AppBundle\Interfaces\Request\Request;

class UserTimeLine implements Request
{
	private $screenName;
	private $count;

	public function __construct(string $screenName, int $count = null) 
	{
		$this->screenName = $screenName;
		$this->count = $count;
	}

	public function getEndPoint(): string
	{
		return 'https://api.twitter.com/1.1/statuses/user_timeline.json';
	}

	public function getRequestType(): string
	{
		return Request::TYPE_GET;
	}

	public function getRawParams(): array
	{
		$params = [
			'screen_name' => $this->screenName
		];

		if (null !== $this->count) {
			$params['count'] = $this->count;
		}

		return $params;
	}
}