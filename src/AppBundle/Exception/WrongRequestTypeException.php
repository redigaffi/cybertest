<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 26/07/17
 * Time: 18:31
 */

namespace AppBundle\Exception;

class WrongRequestTypeException extends \Exception
{
	protected $message = 'The request type is incorrect';
	protected $code = -1;
}