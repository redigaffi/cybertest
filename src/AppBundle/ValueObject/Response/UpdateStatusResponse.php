<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 26/07/17
 * Time: 18:31
 */

namespace AppBundle\ValueObject\Response;

use AppBundle\Exception\ErrorParsingResponseException;
use AppBundle\Interfaces\Response\Response;
use AppBundle\ValueObject\Request\UpdateStatus;

class UpdateStatusResponse implements Response
{
	private $message;

	public function __construct(string $message)
	{
		$this->message = $message;
	}

	public static function fromArray(array $response): UpdateStatusResponse
	{
	    if (!isset($response['text'])) {
	        throw new ErrorParsingResponseException("Missing Field 'text' in ".UpdateStatus::class);
        }

        $updateResponse = new self($response['text']);

        return $updateResponse;
	}
}