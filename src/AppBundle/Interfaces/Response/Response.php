<?php
/**
 * Created by PhpStorm.
 * User: Jordi Hoock Castro
 * Date: 26/07/17
 * Time: 18:31
 */

namespace AppBundle\Interfaces\Response;

interface Response
{
	public static function fromArray(array $response);
}